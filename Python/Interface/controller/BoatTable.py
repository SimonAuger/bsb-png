from folium.utilities import _validate_coordinates

class BoatTable(object):
    """
    Parameters
    ----------
    boats: list of boat, default None
        model.BoatTable.Boat instance

    """
    def __init__(self, boats=None):
        super(BoatTable, self).__init__()
        self._name = 'BoatTable'
        self.boats = boats

    def getLocation(self):
        """
        Return list of tuple(Latitude, Longitude) foreach boats
        """
        locations = None
        for boat in boats:
            locations.append(boat.getLocation())
        return locations

    def getCurrentCoords(self):
        return {'x': 43.512239, 'y': 3.964290}

class Boat(object):
    """
    Parameters
    ----------
    location: tuple, default None
        Latitude and Longitude of Boat (Northing, Easting)
    IsOur: boolean, default False
        If the boat is the FloatBoat
    """

    def __init__(self, location=None, IsOur=False):
        super(Boat, self).__init__()
        self._name = BoatTable
        self.location = _validate_coordinates(location)
        self.IsOur = IsOur

    def getLocation(self):
        return self.location

import MySQLdb as db


#We use the table name 'Obstacle'
#the table contain the following columns :
#0: id
#1: distance
#2: kind
#3: risque
#4: posx
#5: posy

def getAll():
	result = None
	HOST = "mysql-floatboat.alwaysdata.net"
	PORT = 3306
	USER = "floatboat"
	PASSWORD = "2212"
	DB = "floatboat_database"

	try:
	    connection = db.Connection(host=HOST, port=PORT,
	                               user=USER, passwd=PASSWORD, db=DB)

	    dbhandler = connection.cursor()
	    dbhandler.execute("SELECT * from Obstacle")
	    result = dbhandler.fetchall()
	except Exception as e:
	    print (e)

	finally:
	    connection.close()
	    return result

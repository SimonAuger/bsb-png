import serial
import os

def create_pos_file():
	try :
		gps = serial.Serial("/dev/ttyACM0", baudrate = 9600)
		boole = True
		while boole == True:
			line = gps.readline()
			line = str(line)
			data = line.split(",")
			if "$GPRMC" in data[0]:
				if data[2] == "A":
					with open ("position.txt", "w") as pos:
						pos.write("%s, %s\n" % (data[3], data[5]))
						pos.close()
					boole = False
		return True
	except :
		return False
	return False
	

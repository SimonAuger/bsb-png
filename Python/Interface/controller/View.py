try:
    from Tkinter import *
except ImportError:
    from tkinter import *
import folium
#import selenium.webdriver
from selenium import webdriver
import os
import glob
from threading import Thread, Timer
import numpy as np
import gps, Model
from pyvirtualdisplay import Display


class View(object):
    def __init__(self, parent, controller, **options):
        """Create the necessary widgets, and a dict self.values for
        storing the state of the entry widgets in the format:
            {'base': b, 'exponent': e}
        where b and e are floats.

        Requires arguments:
            parent: parent widget
            controller: the object that provides the update_view method
        """

        self.parent = parent
        self.controller = controller

        self.canvas = Canvas(parent, width="868", height = "561")
        self.canvas.pack()
        self.values = {}

        self.img_home = PhotoImage (file="picture/home.png")
        self.img_meteo = PhotoImage(file="picture/weather.png")
        self.img_map = PhotoImage(file="picture/map_icon.png")

        self.img_button_up = PhotoImage(file="picture/button_up.png")
        self.img_button_down = PhotoImage(file="picture/button_down.png")
        self.img_button_right = PhotoImage(file="picture/button_right.png")
        self.img_button_left = PhotoImage(file="picture/button_left.png")

        self.create_fond()
        self.create_icon()
        self.create_canvas()
        self.create_bindings()

        #movement
        self.move_counter = 0 #counter to get the first coord of the B1-Motion
        self.firstx = 0
        self.firsty = 0
        self.difx = 0
        self.dify = 0

        #zoom
        self.zoom_counter = 13
        self.on_map = False

    def create_fond(self):
        self.fond_home = PhotoImage(file="picture/fond.png")
        self.fond = []
        self.fond.append(PhotoImage(file="picture/map.png"))
        self.fond.append(PhotoImage(file="picture/wind.png"))
        self.fond.append(PhotoImage(file="picture/wave_height.png"))
        self.fond.append(PhotoImage(file="picture/temperature_map.png"))
        self.fond.append(PhotoImage(file="picture/precipitation.png"))
        self.fond.append(PhotoImage(file="picture/air_pressure.png"))
        self.fond_counter = 0

    def create_icon(self):
        self.img_icon = []
        self.img_icon.append(PhotoImage(file="picture/wind_icon.png"))                  #0
        self.img_icon.append(PhotoImage(file="picture/wind_icon_selected.png"))         #1
        self.img_icon.append(PhotoImage(file="picture/wave_icon.png"))                  #2
        self.img_icon.append(PhotoImage(file="picture/wave_icon_selected.png"))         #3
        self.img_icon.append(PhotoImage(file="picture/temperature_icon.png"))           #4
        self.img_icon.append(PhotoImage(file="picture/temperature_icon_selected.png"))  #5
        self.img_icon.append(PhotoImage(file="picture/precipitation_icon.png"))         #6
        self.img_icon.append(PhotoImage(file="picture/precipitation_icon_selected.png"))#7
        self.img_icon.append(PhotoImage(file="picture/air_pressure_icon.png"))          #8
        self.img_icon.append(PhotoImage(file="picture/air_pressure_icon_selected.png")) #9
        self.img_counter = 0

    def create_canvas(self):
        self.image_on_canvas = self.canvas.create_image(0, 0, image = self.fond[self.fond_counter], anchor = NW, tag="fond")
        self.image_fond_home = self.canvas.create_image(0, 0, image = self.fond_home, anchor = NW, tag="fond_h")
        self.bouton_home = self.canvas.create_image(270, 90, image=self.img_home, tag="home")
        self.bouton_meteo = self.canvas.create_image(390, 90, image=self.img_meteo, tag="wind")
        self.bouton_map = self.canvas.create_image(510, 90, image=self.img_map, tag="map")
        #boolean for button
        self.wind = None
        self.wave = None
        self.temperature = None
        self.precipitation = None
        self.air = None

        #direction buttons
        self.button_up = self.canvas.create_image(110, 50, image=self.img_button_up, tag="button_up", state="hidden")
        self.button_down = self.canvas.create_image(110, 170, image=self.img_button_down, tag="button_down", state="hidden")
        self.button_right = self.canvas.create_image(170, 110, image=self.img_button_right, tag="button_right", state="hidden")
        self.button_left = self.canvas.create_image(50, 110, image=self.img_button_left, tag="button_left", state="hidden")

    def create_bindings(self):
        self.canvas.tag_bind("wind", "<Button-1>", self.f_wind)
        self.canvas.tag_bind("home", "<Button-1>", self.f_home)
        self.canvas.tag_bind("map", "<Button-1>", self.f_map)
        self.canvas.tag_bind("wave", "<Button-1>", self.f_wave)
        self.canvas.tag_bind("temperature", "<Button-1>", self.f_temperature)
        self.canvas.tag_bind("precipitation", "<Button-1>", self.f_precipitation)
        self.canvas.tag_bind("air", "<Button-1>", self.f_air)
        self.canvas.tag_bind("fond", "<B1-Motion>", self.f_fond)
        self.canvas.tag_bind("fond", "<ButtonRelease-1>", self.s_fond) #reinitialized counter
        #self.canvas.tag_bind("fond", "<MouseWheel>", self.zoom)
        #self.canvas.tag_bind("fond", "<B2-Motion>", self.zoom)
        #self.canvas.tag_bind("fond", "<ButtonRelease-2>", self.zoom_release)
        self.canvas.tag_bind("button_up", "<Button-1>", self.movment)
        self.canvas.tag_bind("button_down", "<Button-1>", self.movment)
        self.canvas.tag_bind("button_left", "<Button-1>", self.movment)
        self.canvas.tag_bind("button_right", "<Button-1>", self.movment)
        self.canvas.bind("<Button-4>", self.zoom)
        self.canvas.bind("<Button-5>", self.zoom)



    def f_wind(self, event):
        self.fond_counter = 1
        self.canvas.itemconfig(self.image_fond_home, state="hidden")

        #x, y = self.pil_image.size
        #self.pil_image.crop((0,0, x/2, y/2)).save('picture/image1.png')
        #self.pil_image.crop((x/2,0, x, y/2)).save('picture/image2.png')
        #self.pil_image.crop((0, y/2, x/2, y)).save('picture/image3.png')
        #self.pil_image.crop((x/2, y/2, x, y)).save('picture/image4.png')

        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        #self.canvas.itemconfig(self.image_on_canvas, image = PhotoImage(file = "picture/wind.png"))

        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        if not self.wind:
            self.wind = self.canvas.create_image(50, 50, image=self.img_icon[1], tag="wind")
            self.wave = self.canvas.create_image(50, 110, image=self.img_icon[2], tag="wave")
            self.temperature = self.canvas.create_image(50, 170, image=self.img_icon[4], tag="temperature")
            self.precipitation = self.canvas.create_image(50, 230, image=self.img_icon[6], tag="precipitation")
            self.air = self.canvas.create_image(50, 290, image=self.img_icon[8], tag="air")
        else:
            self.canvas.itemconfig(self.wind, image = self.img_icon[1])
            self.canvas.itemconfig(self.wave, image = self.img_icon[2])
            self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
            self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
            self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_wave(self, event):
        self.fond_counter = 2
        self.canvas.itemconfig(self.image_fond_home, state="hidden")
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[3])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
        self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_temperature(self, event):
        self.fond_counter = 3
        self.canvas.itemconfig(self.image_fond_home, state="hidden")
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[2])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[5])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
        self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_precipitation(self, event):
        self.fond_counter = 4
        self.canvas.itemconfig(self.image_fond_home, state="hidden")
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[2])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[7])
        self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_air(self, event):
        self.fond_counter = 5
        self.canvas.itemconfig(self.image_fond_home, state="hidden")
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[2])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
        self.canvas.itemconfig(self.air, image = self.img_icon[9])

    def f_map(self, event):
        self.fond_counter = 0
        self.on_map = True
        self.canvas.itemconfig(self.image_fond_home, state="hidden")
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.itemconfig(self.button_up, state="normal")
        self.canvas.itemconfig(self.button_down, state="normal")
        self.canvas.itemconfig(self.button_left, state="normal")
        self.canvas.itemconfig(self.button_right, state="normal")
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        if self.wind:
            self.canvas.delete(self.wind)
            self.canvas.delete(self.wave)
            self.canvas.delete(self.temperature)
            self.canvas.delete(self.precipitation)
            self.canvas.delete(self.air)
            self.wind = None

    def f_home(self, event):
        self.on_map = False
        self.canvas.itemconfig(self.image_fond_home, state="normal")
        self.canvas.itemconfig(self.button_up, state="hidden")
        self.canvas.itemconfig(self.button_down, state="hidden")
        self.canvas.itemconfig(self.button_left, state="hidden")
        self.canvas.itemconfig(self.button_right, state="hidden")
        self.canvas.coords(self.bouton_home, 270, 90)
        self.canvas.coords(self.bouton_meteo, 390, 90)
        self.canvas.coords(self.bouton_map, 510, 90)
        if self.wind:
            self.canvas.delete(self.wind)
            self.canvas.delete(self.wave)
            self.canvas.delete(self.temperature)
            self.canvas.delete(self.precipitation)
            self.canvas.delete(self.air)
            self.wind = None

    def f_fond(self, event):
        #print(event.x, event.y)
        if self.move_counter == 0:
            self.firstx = event.x
            self.firsty = event.y
        self.canvas.move("fond", event.x - self.firstx - self.difx, event.y - self.firsty - self.dify)
        #print(event.x - self.firstx - self.difx)
        self.move_counter = self.move_counter + 1
        self.difx = event.x - self.firstx
        self.dify = event.y - self.firsty

    def s_fond(self, event):
        self.move_counter = 0
        #print(self.values['x'] , self.values['y'] )
        print(self.canvas.coords("fond"))
        #self.set_map(True)
        self.difx = 0
        self.dify = 0
        self.scroll_canvas(self.canvas.coords("fond"))

    def scroll_canvas(self, coords):
        test = False
        if coords[1] > 0: #deplacement vers le haut
            self.values['x'] = self.values['x'] + 0.0001*coords[1]*(np.exp(np.exp(7/self.zoom_counter)))
            test = True
        if coords[1] < -300: #deplacement vers le bas
            self.values['x'] = self.values['x'] + 0.0001*(coords[1]+300)*(np.exp(np.exp(7/self.zoom_counter)))
            test = True
        if coords[0] > 0: #deplacement vers la gauche
            self.values['y'] = self.values['y'] - 0.0001*coords[0]*(np.exp(np.exp(7/self.zoom_counter)))
            test = True
        if coords[0] < -600: #deplacement vers la droite
            self.values['y'] = self.values['y'] - 0.0001*(coords[0]+600)*(np.exp(np.exp(7/self.zoom_counter)))
            test = True
        if test:
            self.canvas.move("fond", -coords[0],  -coords[1])
            self.set_map(True)
            test = False

    def zoom(self, event):
        zoom_temp = self.zoom_counter
        if self.on_map:
            if event.num == 4 and self.zoom_counter < 18:
                self.zoom_counter = self.zoom_counter + 1
            if event.num == 5 and self.zoom_counter > 0 :
                self.zoom_counter = self.zoom_counter - 1
        print(self.zoom_counter)
        if zoom_temp != self.zoom_counter:
            #defini la fonction a excecuter en arriere plan
            try:
                t = Timer(1.0, self.set_map, (True,))
                t.start()
            except:
                print("To much threads for the moment")

    def zoom_release(self, event):
        #print(self.zoom_counter)
        self.zoom_counter = 0


    def set_values(self, values):
        self.values['x'] = values['x']
        self.values['y'] = values['y']

    def set_map(self, mode=False):
        """
        if mode = false => initialization
        mode= true => change map during movement
        """
        data = self.getPositionBoat();
        model = Model.getAll();
        self.macarte = folium.Map(location=[self.values['x'], self.values['y']], zoom_start=self.zoom_counter)
        folium.Marker(location=[data[0], data[1]], popup=folium.Popup('Portland, OR')).add_to(self.macarte)
        if model:
            for item in model:
                couleur = ""
                if item[3] == '3':
                    couleur = "red"
                if item[3] == "2":
                    couleur = "orange"
                if item[3] == "1":
                    couleur = "green"
                folium.Marker(location=[item[4], item[5]], popup=item[2], icon=folium.Icon(color=couleur, icon='info-sign')).add_to(self.macarte)
        self.macarte.save('Map/mediteranean.html')
        #defini la fonction a excecuter en arriere plan
        self.get_screen(mode)

    def get_screen(self, mode=False):
        temp_name = "file:/home/benjamin/controller/Map/mediteranean.html"
        # open in webpage
        #driver = webdriver.PhantomJS(service_log_path=os.path.devnull)
        display = Display(visible=0, size=(800,800))
        display.start()
        driver = webdriver.Firefox()
        driver.get(temp_name)
        driver.set_window_size(1000, 700)
        driver.execute_script("document.body.style.width = '100%';")
        driver.implicitly_wait(3)
        save_name = 'picture/m2.png'
        driver.get_screenshot_as_file(save_name)
        #driver.save_screenshot(save_name)
        driver.quit()
        self.fond_counter = 0
        self.fond[self.fond_counter] = PhotoImage(file="picture/m2.png")
        if mode==True:
            self.canvas.itemconfig(self.image_on_canvas, self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter]))

    def movment(self, event):
        if(event.x > 80 and event.x < 140 and event.y > 20 and event.y < 80): #button up
            #self.set_map(True)
            self.values['x'] = self.values['x'] + 0.02*(np.exp(np.exp(7/self.zoom_counter)))
        if(event.x > 80 and event.x < 140 and event.y > 140 and event.y < 200): #button down
            self.values['x'] = self.values['x'] - 0.02*(np.exp(np.exp(7/self.zoom_counter)))
        if(event.x > 140 and event.x < 200 and event.y > 80 and event.y < 140): #button right
            self.values['y'] = self.values['y'] + 0.06*(np.exp(np.exp(7/self.zoom_counter)))
        if(event.x > 20 and event.x < 80 and event.y > 80 and event.y < 140): #button left
            self.values['y'] = self.values['y'] - 0.06*(np.exp(np.exp(7/self.zoom_counter)))
        self.set_map(True)

    def getPositionBoat(self):
        #create a file position.txt that contain {latittude} {longitude}
        if gps.create_pos_file(): 
            with open("position.txt", "r") as pos:
                line = pos.read()

                #split data into array
                data = line.split(", ")

                #The . is at the wrong place we will put it at the 3rd character
                temp = data[0].split(".")
                data[0] = temp[0] + temp[1]
        
                latittude = ""
                for i in range(len(data[0])):
                    if i == 2:
                        character = data[0][i]
                        latittude += "."
                        latittude += character
                    else :
                        latittude += data[0][i]

                while latittude[0] == "0":
                    latittude = latittude[1:]
                if latittude[0] == ".":
                    latittude = "0" + latittude
                data[0] = float(latittude)


                temp = data[1].split(".")
                data[1] = temp[0] + temp[1]
                
                longitude = ""
                for i in range(len(data[1])):
                    if i == 2:
                        character = data[1][i]
                        longitude += "."
                        longitude += character
                    else :
                        longitude += data[1][i]

                while longitude[0] == "0":
                    longitude = longitude[1:]
                if longitude[0] == ".":
                    longitude = "0" + longitude
                data[1] = float(longitude)
                return data
        else :
            #we return default position
            data = [43.512239, 3.964290]
            return data


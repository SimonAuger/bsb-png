from BoatTable import BoatTable
from View import View
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

class Controller(object):
    """
    Parameters
    ----------
        root: a tkinter.Tk() object, to be used by the view.
    """

    def __init__(self, root):
        super(Controller, self).__init__()
        self._name = 'Controller'
        self.boatmodel = BoatTable()
        self.default_values = {'x': 43.512239, 'y': 3.964290}
        self.view = View(root, self)
        self.initialize_view()

    def initialize_view(self):
        """Initialize the view's entry widgets and plot canvas."""
        data = self.view.getPositionBoat();
        self.default_values = {'x': data[0], 'y': data[1]}
        self.view.set_values(self.default_values)
        self.view.set_map()
        self.update_view()

    def update_view(self):
        """Request coordonates of boat from the model, and update the view's plot
        with the new data.
        """
        self.get_data()
        #self.update_view_map()

    def get_data(self):
        """
        Send the coordonates to the model and store the plot data returned.
        """
        self.coords = self.boatmodel.getCurrentCoords()

    def update_view_map(self):
        """
        Call view's methods to clear and reload the map image with the new coords
        """
        #self.view.canvas.clear()
        #self.view.canvas.plot(*self.plot_data)



root = tk.Tk()
app = Controller(root)
root.mainloop()

import glob
from PIL import Image
#from selenium import webdriver
import selenium.webdriver
import os

# get a list of all the files to open

temp_name = "file:///C:/Users/benji/OneDrive/Documents/ING4/PPE/Versionning/bsd/Python/Map/mediteranean.html"

# open in webpage
#driver = webdriver.Chrome("D:\Téléchargement\chromedriver_win32\chromedriver.exe")
driver = selenium.webdriver.PhantomJS(
            service_log_path=os.path.devnull
        )
driver.get(temp_name)
driver.maximize_window()
driver.execute_script("document.body.style.width = '100%';")
driver.implicitly_wait(3)
save_name = 'picture/m2.png'
driver.get_screenshot_as_file(save_name)
#driver.save_screenshot(save_name)
driver.quit()


# crop as required
img = Image.open(save_name)
box = (1, 1, 1000, 1000)
area = img.crop(box)
area.save('picture/cropped_image.png')

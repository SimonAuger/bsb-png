from tkinter import *
from PIL import Image

class MainWindow():
    def __init__(self, main):
        # canvas for image
        self.canvas = Canvas(main, width="868", height = "561")
        self.canvas.pack()

        # image
        self.pil_image = Image.open("picture/map.png")

        ########################test zoom ##############################


        self.fond = []
        self.fond.append(PhotoImage(file="picture/fond.png"))
        self.fond.append(PhotoImage(file="picture/map.png"))
        self.fond.append(PhotoImage(file="picture/wind.png"))
        self.fond.append(PhotoImage(file="picture/wave_height.png"))
        self.fond.append(PhotoImage(file="picture/temperature_map.png"))
        self.fond.append(PhotoImage(file="picture/precipitation.png"))
        self.fond.append(PhotoImage(file="picture/air_pressure.png"))
        self.fond_counter = 0

        self.img_home = PhotoImage (file="picture/home.png")
        self.img_meteo = PhotoImage(file="picture/weather.png")
        self.img_map = PhotoImage(file="picture/map_icon.png")

        self.img_icon = []
        self.img_icon.append(PhotoImage(file="picture/wind_icon.png"))                  #0
        self.img_icon.append(PhotoImage(file="picture/wind_icon_selected.png"))         #1
        self.img_icon.append(PhotoImage(file="picture/wave_icon.png"))                  #2
        self.img_icon.append(PhotoImage(file="picture/wave_icon_selected.png"))         #3
        self.img_icon.append(PhotoImage(file="picture/temperature_icon.png"))           #4
        self.img_icon.append(PhotoImage(file="picture/temperature_icon_selected.png"))  #5
        self.img_icon.append(PhotoImage(file="picture/precipitation_icon.png"))         #6
        self.img_icon.append(PhotoImage(file="picture/precipitation_icon_selected.png"))#7
        self.img_icon.append(PhotoImage(file="picture/air_pressure_icon.png"))          #8
        self.img_icon.append(PhotoImage(file="picture/air_pressure_icon_selected.png")) #9
        self.img_counter = 0

        # set first image on canvas
        self.image_on_canvas = self.canvas.create_image(0, 0, image = self.fond[self.fond_counter], anchor = NW, tag="fond")
        self.bouton_home = self.canvas.create_image(270, 90, image=self.img_home, tag="home")
        self.bouton_meteo = self.canvas.create_image(390, 90, image=self.img_meteo, tag="wind")
        self.bouton_map = self.canvas.create_image(510, 90, image=self.img_map, tag="map")

        self.canvas.tag_bind("wind", "<Button-1>", self.f_wind)
        self.canvas.tag_bind("home", "<Button-1>", self.f_home)
        self.canvas.tag_bind("map", "<Button-1>", self.f_map)
        self.canvas.tag_bind("wave", "<Button-1>", self.f_wave)
        self.canvas.tag_bind("temperature", "<Button-1>", self.f_temperature)
        self.canvas.tag_bind("precipitation", "<Button-1>", self.f_precipitation)
        self.canvas.tag_bind("air", "<Button-1>", self.f_air)
        self.canvas.tag_bind("fond", "<B1-Motion>", self.f_fond)
        self.canvas.tag_bind("fond", "<ButtonRelease-1>", self.s_fond) #reinitialized counter

        #boolean for button
        self.wind = None
        self.wave = None
        self.temperature = None
        self.precipitation = None
        self.air = None

        #movement
        self.move_counter = 0 #counter to get the first coord of the B1-Motion
        self.firstx = 0
        self.firsty = 0
        self.difx = 0
        self.dify = 0


        # button to change image
        # self.button = Button(main, text="Change", command=self.onButton)
        # self.button.pack()

    def f_wind(self, event):
        self.fond_counter = 2

        #x, y = self.pil_image.size
        #self.pil_image.crop((0,0, x/2, y/2)).save('picture/image1.png')
        #self.pil_image.crop((x/2,0, x, y/2)).save('picture/image2.png')
        #self.pil_image.crop((0, y/2, x/2, y)).save('picture/image3.png')
        #self.pil_image.crop((x/2, y/2, x, y)).save('picture/image4.png')

        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        #self.canvas.itemconfig(self.image_on_canvas, image = PhotoImage(file = "picture/wind.png"))

        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        if not self.wind:
            self.wind = self.canvas.create_image(50, 50, image=self.img_icon[1], tag="wind")
            self.wave = self.canvas.create_image(50, 110, image=self.img_icon[2], tag="wave")
            self.temperature = self.canvas.create_image(50, 170, image=self.img_icon[4], tag="temperature")
            self.precipitation = self.canvas.create_image(50, 230, image=self.img_icon[6], tag="precipitation")
            self.air = self.canvas.create_image(50, 290, image=self.img_icon[8], tag="air")
        else:
            self.canvas.itemconfig(self.wind, image = self.img_icon[1])
            self.canvas.itemconfig(self.wave, image = self.img_icon[2])
            self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
            self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
            self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_wave(self, event):
        self.fond_counter = 3
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[3])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
        self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_temperature(self, event):
        self.fond_counter = 4
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[2])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[5])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
        self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_precipitation(self, event):
        self.fond_counter = 5
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[2])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[7])
        self.canvas.itemconfig(self.air, image = self.img_icon[8])

    def f_air(self, event):
        self.fond_counter = 6
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        self.canvas.itemconfig(self.wind, image = self.img_icon[0])
        self.canvas.itemconfig(self.wave, image = self.img_icon[2])
        self.canvas.itemconfig(self.temperature, image = self.img_icon[4])
        self.canvas.itemconfig(self.precipitation, image = self.img_icon[6])
        self.canvas.itemconfig(self.air, image = self.img_icon[9])

    def f_map(self, event):
        self.fond_counter = 1
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 450)
        self.canvas.coords(self.bouton_meteo, 390, 450)
        self.canvas.coords(self.bouton_map, 510, 450)
        if self.wind:
            self.canvas.delete(self.wind)
            self.canvas.delete(self.wave)
            self.canvas.delete(self.temperature)
            self.canvas.delete(self.precipitation)
            self.canvas.delete(self.air)
            self.wind = None

    def f_home(self, event):
        self.fond_counter = 0
        self.canvas.itemconfig(self.image_on_canvas, image = self.fond[self.fond_counter])
        self.canvas.coords(self.bouton_home, 270, 90)
        self.canvas.coords(self.bouton_meteo, 390, 90)
        self.canvas.coords(self.bouton_map, 510, 90)
        if self.wind:
            self.canvas.delete(self.wind)
            self.canvas.delete(self.wave)
            self.canvas.delete(self.temperature)
            self.canvas.delete(self.precipitation)
            self.canvas.delete(self.air)
            self.wind = None

    def f_fond(self, event):
        if self.fond_counter:
            #print(event.x, event.y)
            if self.move_counter == 0:
                self.firstx = event.x
                self.firsty = event.y
            self.canvas.move("fond", event.x - self.firstx - self.difx, event.y - self.firsty - self.dify)
            self.move_counter = self.move_counter + 1
            self.difx = event.x - self.firstx
            self.dify = event.y - self.firsty

    def s_fond(self, event):
        self.move_counter = 0
        self.difx = 0
        self.dify = 0


root = Tk()
MainWindow(root)
root.mainloop()

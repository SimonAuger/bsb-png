from PIL import Image
 
def chargez_img(filename, resize=None):
    img = Image.open(filename)
    if resize is not None:
        img = img.resize(resize, Image.ANTIALIAS)
    return img
 
 
photo = chargez_img('tigre.jpg')

 
x1, y1, x2, y2 = 400, 600, 600, 800
photo_crop = photo.crop((x1, y1, x2, y2))
 
photo_crop.save('new_image_cropped.jpg')
 
photo.close()
photo_crop.close()

package PPE.Model;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.ArrayList;

import org.vanilladb.core.remote.jdbc.JdbcDriver;

/*
 * TABLE student (id INT, name VARCHAR(20))
 * 
 */

public class StudentTable {
	private static String table_name = "student";
	
	/* return tuples in a table 
	 */
	public static boolean show() {
		try {
			String sql = "SELECT id, name FROM student";
			Operation.doQuery1(sql.trim(), "SELECT");
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static int insert(int id, String name) {
		String sql = "INSERT INTO student (id, name) values ("+id+", '"+name+"')";
		return Operation.doUpdate(sql.trim());
	}
	
	public static void create() throws SQLException {
		
		String sql = "CREATE TABLE student (id INT, name VARCHAR(20))";
		if(!Operation.exist("STUDENT")) Operation.doUpdate(sql.trim());
	}
	
	
	public static void drop() {
		String sql = "DROP TABLE IF EXISTS student";
		Operation.doUpdate(sql.trim());
	}
	
	
}
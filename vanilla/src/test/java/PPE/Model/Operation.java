package PPE.Model;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

import org.vanilladb.core.remote.jdbc.JdbcDriver;
import org.vanilladb.core.server.VanillaDb;
import org.vanilladb.core.sql.IntegerConstant;
import org.vanilladb.core.sql.Schema;
import org.vanilladb.core.sql.Type;
import org.vanilladb.core.sql.VarcharConstant;
import org.vanilladb.core.storage.metadata.CatalogMgr;
import org.vanilladb.core.storage.metadata.TableInfo;
import org.vanilladb.core.storage.record.RecordFile;
import org.vanilladb.core.storage.tx.Transaction;

public class Operation {
	private static Connection conn = null;
	
	public Operation() throws Exception {
		VanillaDb.init("studentdb");
	}
	
	protected static boolean doQuery(String cmd,String cmdf) {
		try {
			Driver d = new JdbcDriver();
			conn = d.connect("jdbc:vanilladb://localhost", null);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(cmd);
			ResultSetMetaData md = rs.getMetaData();
			int numcols = md.getColumnCount();
			int totalwidth = 0;

			// print header
			for (int i = 1; i <= numcols; i++) {
				int width = md.getColumnDisplaySize(i);
				totalwidth += width;
				String fmt = "%" + width + "s";
				if (cmdf.startsWith("EXPLAIN"))
					System.out.format("%s", md.getColumnName(i));
				else
					System.out.format(fmt, md.getColumnName(i));
			}

			System.out.println();
			for (int i = 0; i < totalwidth; i++)
				System.out.print("-");
			if (!cmdf.startsWith("EXPLAIN"))
				System.out.println();

			rs.beforeFirst();
			// print records
			while (rs.next()) {
				for (int i = 1; i <= numcols; i++) {
					String fldname = md.getColumnName(i);
					int fldtype = md.getColumnType(i);
					String fmt = "%" + md.getColumnDisplaySize(i);
					if (fldtype == Types.INTEGER)
						System.out.format(fmt + "d", rs.getInt(fldname));
					else if (fldtype == Types.BIGINT)
						System.out.format(fmt + "d", rs.getLong(fldname));
					else if (fldtype == Types.DOUBLE)
						System.out.format(fmt + "f", rs.getDouble(fldname));
					else
						System.out.format(fmt + "s", rs.getString(fldname));
				}
				System.out.println();
			}
			rs.close();
			return true;
		} catch (SQLException e) {
			System.out.println("SQL Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	public static void doQuery1(String cmd,String cmdf) {
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(cmd);
			ResultSetMetaData md = rs.getMetaData();
			int numcols = md.getColumnCount();
			int totalwidth = 0;

			// print header
			for (int i = 1; i <= numcols; i++) {
				int width = md.getColumnDisplaySize(i);
				totalwidth += width;
				String fmt = "%" + width + "s";
				if (cmdf.startsWith("EXPLAIN"))
					System.out.format("%s", md.getColumnName(i));
				else
					System.out.format(fmt, md.getColumnName(i));
			}

			System.out.println();
			for (int i = 0; i < totalwidth; i++)
				System.out.print("-");
			if (!cmdf.startsWith("EXPLAIN"))
				System.out.println();

			rs.beforeFirst();
			// print records
			while (rs.next()) {
				for (int i = 1; i <= numcols; i++) {
					String fldname = md.getColumnName(i);
					int fldtype = md.getColumnType(i);
					String fmt = "%" + md.getColumnDisplaySize(i);
					if (fldtype == Types.INTEGER)
						System.out.format(fmt + "d", rs.getInt(fldname));
					else if (fldtype == Types.BIGINT)
						System.out.format(fmt + "d", rs.getLong(fldname));
					else if (fldtype == Types.DOUBLE)
						System.out.format(fmt + "f", rs.getDouble(fldname));
					else
						System.out.format(fmt + "s", rs.getString(fldname));
				}
				System.out.println();
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception: " + e.getMessage());
			e.printStackTrace();
		}
	}

	protected static int doUpdate(String cmd) {
		try {
			Driver d = new JdbcDriver();
			conn = d.connect("jdbc:vanilladb://localhost", null);
			Statement stmt = conn.createStatement();
			int howmany = stmt.executeUpdate(cmd);
			System.out.println(howmany + " records processed");
			return howmany;
		} catch (SQLException e) {
			System.out.println("SQL Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}
	
	protected static boolean exist(String table_name) {
		try {
			Transaction tx = VanillaDb.txMgr().newTransaction(Connection.TRANSACTION_SERIALIZABLE, false);
			TableInfo ti = VanillaDb.catalogMgr().getTableInfo(table_name, tx);
			if(ti != null) return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void read() throws Exception {
		try {
			Transaction tx1 = VanillaDb.txMgr().newTransaction(
			Connection.TRANSACTION_SERIALIZABLE, false);
			TableInfo ti = VanillaDb.catalogMgr().getTableInfo("dept", tx1);
			RecordFile rf = ti.open(tx1, true);
			rf.beforeFirst();
			// Part 1: reads records and delete records
			while (rf.next()) {
				if (rf.getVal("did").equals(new IntegerConstant(50)))
				{
					System.out.println(rf.toString());
					rf.delete();
				}
					
			}
			rf.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void write() throws Exception {
		Transaction tx1 = VanillaDb.txMgr().newTransaction(
		Connection.TRANSACTION_SERIALIZABLE, false);
		TableInfo ti = VanillaDb.catalogMgr().getTableInfo("dept", tx1);
		RecordFile rf = ti.open(tx1, true);
		rf.beforeFirst();
		// Part 2: insert new record
		rf = ti.open(tx1, true);
		for (int id = 0; id < 100; id++) {
			rf.insert();
			rf.setVal("did", new IntegerConstant(id));
			rf.setVal("dname", new VarcharConstant("student" + id));
		}
	}
	
	public void create() throws Exception {
		CatalogMgr catalogMgr = VanillaDb.catalogMgr();
		// Create dept table
		Transaction tx1 = VanillaDb.txMgr().newTransaction(
		Connection.TRANSACTION_SERIALIZABLE, false);
		Schema sch = new Schema();
		sch.addField("did", Type.INTEGER);
		sch.addField("dname", Type.VARCHAR(20));
		catalogMgr.createTable("dept", sch, tx1);
		tx1.commit();
	}
}
